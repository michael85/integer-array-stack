//
//  IntArrayStack.cpp
//

#include "IntArrayStack.h"
#include "Flags.h"

/* **************************************************************** */

#if CONSTRUCTOR || ALL
IntArrayStack::IntArrayStack()
{
	top = -1;
	maxSize = 1;
	stack = new int[maxSize];
	
}
#endif

/* **************************************************************** */

#if DESTRUCTOR || ALL
IntArrayStack::~IntArrayStack()
{
	delete[] stack;
}
#endif

/* **************************************************************** */

#if PUSH || ALL
void IntArrayStack::push(int a)
{
	if(getSize() == maxSize)
	{
		maxSize*=2;
		resize(maxSize);
	}
	top++;
	stack[top] = a;
}
#endif

/* **************************************************************** */

#if POP || ALL
int IntArrayStack::pop()
{
	if(!isEmpty())
	{
		return -1;
	}

	if((maxSize/3) > top)
	{
		maxSize = maxSize / 2;
		resize(maxSize);
	}
	
	int poped = stack[top];
	top--;
	return poped;
}
#endif

/* **************************************************************** */

#if PEEK || ALL
int IntArrayStack::peek()
{
	int peek = stack[top];
	if(peek == -1)
	{
		return -1;
	}

	return peek;
}
#endif

/* **************************************************************** */

#if ISEMPTY || ALL
bool IntArrayStack::isEmpty()
{
	if(stack[top] < 0)
		return false;

	return true;
}
#endif

/* **************************************************************** */

#if EMPTYSTACK || ALL
void IntArrayStack::emptyStack()
{
	top = -1;
	maxZice = 1;
}
#endif

/* **************************************************************** */

#if RESIZE || ALL
// TODO: resize()
#endif

/* **************************************************************** */
// Do NOT modify anything below this line
/* **************************************************************** */

#ifndef BUILD_LIB
void IntArrayStack::printStack()
{
    std::cout << std::endl;
    std::cout << "Current array size: " << maxSize << std::endl;
    std::cout << "Number of ints in stack: " << top+1 << std::endl;
    for (int i=top; i >= 0; i--)
    {
        std::cout << stack[i] << " ";
    }
    std::cout << std::endl;
}


int IntArrayStack::getCapacity()
{
  return maxSize;
}


int IntArrayStack::getSize()
{
  return top+1;
}


void IntArrayStack::toArray(int* arr)
{
  for (int i=top; i >= 0; i--)
  {
    arr[i] = stack[i];
  }
}
#endif

/* **************************************************************** */


